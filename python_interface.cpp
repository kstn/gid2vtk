/*
    Data Reader and Inspector for GiD binary post results
    Copyright (C) 2016  Hoang-Giang Bui

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// External includes
#include <boost/python.hpp>

// Project includes
#include "gid_binary_reader.h"
#include "vtk_binary_encoder.h"

namespace GiD2VTK
{

/////////////////////////////////////////////////////////////
/// GiD BINARY READER                                     ///
/////////////////////////////////////////////////////////////
boost::python::list GiDBinaryReader_GetMeshesName(GiDBinaryReader& dummy)
{
    std::vector<std::string> meshes_name = dummy.GetMeshesName();
    boost::python::list output;
    for(std::size_t i = 0; i < meshes_name.size(); ++i)
        output.append(meshes_name[i]);
    return output;
}

boost::python::list GiDBinaryReader_ReadMesh(GiDBinaryReader& dummy, const std::string Name)
{
    std::map<int, std::vector<double> > rCoordinates;
    std::map<int, std::vector<int> > rConnectivities;

    dummy.ReadMesh(Name, rCoordinates, rConnectivities);

    boost::python::dict DictCoordinates;
    for(std::map<int, std::vector<double> >::iterator it = rCoordinates.begin(); it != rCoordinates.end(); ++it)
    {
        boost::python::list py_list;
        py_list.append(it->second[0]);
        py_list.append(it->second[1]);
        py_list.append(it->second[2]);
        DictCoordinates[it->first] = py_list;
    }

    boost::python::dict DictConnectivities;
    for(std::map<int, std::vector<int> >::iterator it = rConnectivities.begin(); it != rConnectivities.end(); ++it)
    {
        boost::python::list py_list;
        for(std::size_t i = 0; i < it->second.size(); ++i)
            py_list.append(it->second[i]);
        DictConnectivities[it->first] = py_list;
    }

    boost::python::list output;
    output.append(DictCoordinates);
    output.append(DictConnectivities);
    return output;
}

boost::python::list GiDBinaryReader_ReadNodalScalarValues(GiDBinaryReader& dummy, const std::string Name)
{
    std::vector<double> step_list;
    std::map<std::size_t, std::vector<double> > Values;

    dummy.ReadNodalScalarValues(Name, step_list, Values);

    boost::python::list ListStep;
    for(std::size_t i = 0; i < step_list.size(); ++i)
        ListStep.append(step_list[i]);

    boost::python::dict DictValues;
    for(std::map<std::size_t, std::vector<double> >::iterator it = Values.begin(); it != Values.end(); ++it)
    {
        boost::python::list py_list;
        for(std::size_t i = 0; i < it->second.size(); ++i)
            py_list.append(it->second[i]);
        DictValues[it->first] = py_list;
    }

    boost::python::list output;
    output.append(ListStep);
    output.append(DictValues);
    return output;
}

boost::python::list GiDBinaryReader_ReadNodalVectorValues(GiDBinaryReader& dummy, const std::string Name, std::size_t vector_size)
{
    std::vector<double> step_list;
    std::map<std::size_t, std::vector<std::vector<double> > > Values;

    dummy.ReadNodalVectorValues(Name, step_list, Values, vector_size);

    boost::python::list ListStep;
    for(std::size_t i = 0; i < step_list.size(); ++i)
        ListStep.append(step_list[i]);

    boost::python::dict DictValues;
    for(std::map<std::size_t, std::vector<std::vector<double> > >::iterator it = Values.begin(); it != Values.end(); ++it)
    {
        boost::python::list py_list;
        for(std::size_t i = 0; i < it->second.size(); ++i)
        {
            boost::python::list py_list2;
            for(std::size_t j = 0; j < it->second[i].size(); ++j)
                py_list2.append(it->second[i][j]);
            py_list.append(py_list2);
        }
        DictValues[it->first] = py_list;
    }

    boost::python::list output;
    output.append(ListStep);
    output.append(DictValues);
    return output;
}

boost::python::list GiDBinaryReader_ReadGaussPointScalarValues(GiDBinaryReader& dummy, const std::string Name, const std::string GpName)
{
    std::vector<double> step_list;
    std::map<std::size_t, std::vector<std::vector<double> > > Values;

    dummy.ReadGaussPointScalarValues(Name, GpName, step_list, Values);

    boost::python::list ListStep;
    for(std::size_t i = 0; i < step_list.size(); ++i)
        ListStep.append(step_list[i]);

    boost::python::dict DictValues;
    for(std::map<std::size_t, std::vector<std::vector<double> > >::iterator it = Values.begin(); it != Values.end(); ++it)
    {
        boost::python::list py_list;
        for(std::size_t i = 0; i < it->second.size(); ++i)
        {
            boost::python::list py_list2;
            for(std::size_t j = 0; j < it->second[i].size(); ++j)
                py_list2.append(it->second[i][j]);
            py_list.append(py_list2);
        }
        DictValues[it->first] = py_list;
    }

    boost::python::list output;
    output.append(ListStep);
    output.append(DictValues);
    return output;
}

/////////////////////////////////////////////////////////////

void VTKBinaryEncoder_WriteInt(VTKBinaryEncoder& dummy, PyObject* pyfile, boost::python::list& data)
{
    FILE * cfile = PyFile_AsFile( pyfile );

    std::vector<int> int_data(len(data));
    for(std::size_t i = 0; i < len(data); ++i)
        int_data[i] = boost::python::extract<int>(data[i]);

    dummy.WriteInt(cfile, int_data);
}

void VTKBinaryEncoder_WriteUInt8(VTKBinaryEncoder& dummy, PyObject* pyfile, boost::python::list& data)
{
    FILE * cfile = PyFile_AsFile( pyfile );

    std::vector<uint8_t> int_data(len(data));
    for(std::size_t i = 0; i < len(data); ++i)
        int_data[i] = boost::python::extract<uint8_t>(data[i]);

    dummy.WriteUInt8(cfile, int_data);
}

void VTKBinaryEncoder_WriteFloat(VTKBinaryEncoder& dummy, PyObject* pyfile, boost::python::list& data)
{
    FILE * cfile = PyFile_AsFile( pyfile );

    std::vector<float> int_data(len(data));
    for(std::size_t i = 0; i < len(data); ++i)
        int_data[i] = boost::python::extract<float>(data[i]);

    dummy.WriteFloat(cfile, int_data);
}


/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
using namespace boost::python;

BOOST_PYTHON_MODULE(GiD2VTK)
{
    class_<GiDBinaryReader, GiDBinaryReader::Pointer, boost::noncopyable>(
        "GiDBinaryReader", init<const std::string&>()
    )
    .def("ReadMesh", GiDBinaryReader_ReadMesh)
    .def("GetMeshesName", GiDBinaryReader_GetMeshesName)
    .def("ReadNodalScalarValues", GiDBinaryReader_ReadNodalScalarValues)
    .def("ReadNodalVectorValues", GiDBinaryReader_ReadNodalVectorValues)
    .def("ReadGaussPointScalarValues", GiDBinaryReader_ReadGaussPointScalarValues)
    .def(self_ns::str(self))
    ;

    class_<VTKBinaryEncoder, VTKBinaryEncoder::Pointer, boost::noncopyable>(
        "VTKBinaryEncoder", init<>()
    )
    .def("WriteInt", VTKBinaryEncoder_WriteInt)
    .def("WriteUInt8", VTKBinaryEncoder_WriteUInt8)
    .def("WriteFloat", VTKBinaryEncoder_WriteFloat)
    .def("Compressor", &VTKBinaryEncoder::Compressor)
    .def("test1", &VTKBinaryEncoder::test1)
    ;
}

} // Namespace GiD2VTK

