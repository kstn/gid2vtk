/*
    Data Reader and Inspector for GiD binary post results
    Copyright (C) 2016  Hoang-Giang Bui

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//
//   Project Name:        VTK Binary Encoder
//   Project Description: Encode the data from text to binary for vtkZLibDataCompressor
//   Last Modified by:    $Author: hbui $
//   Date:                $Date: Feb 3, 2016 $
//   Revision:            $Revision: 1.0 $
//
//


#if !defined(VTK_BINARY_ENCODER_H_INCLUDED)
#define VTK_BINARY_ENCODER_H_INCLUDED


// System includes
#include <string>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <vector>


// External includes
#include "boost/smart_ptr.hpp"
#include "boost/algorithm/string.hpp"
#include "libb64/libb64.h"
#include "zlib.h"
#include "zconf.h"

#define WATCH(a) std::cout << #a << ": " << a << std::endl;

namespace GiD2VTK
{

/**********************************************************************/
/************b64.c*****************************************************/
/**********************************************************************/

///*
//** Translation Table as described in RFC1113
//*/
//static const char cb64[]="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

///*
//** encodeblock
//**
//** encode 3 8-bit binary bytes as 4 '6-bit' characters
//*/
//static void encodeblock( unsigned char *in, unsigned char *out, int len )
//{
//    out[0] = (unsigned char) cb64[ (int)(in[0] >> 2) ];
//    out[1] = (unsigned char) cb64[ (int)(((in[0] & 0x03) << 4) | ((in[1] & 0xf0) >> 4)) ];
//    out[2] = (unsigned char) (len > 1 ? cb64[ (int)(((in[1] & 0x0f) << 2) | ((in[2] & 0xc0) >> 6)) ] : '=');
//    out[3] = (unsigned char) (len > 2 ? cb64[ (int)(in[2] & 0x3f) ] : '=');
//}

int vtk_write_binary(FILE * vtkfile, char *numeric_data, size_t byte_length);

void zlib_check(int val);

int vtk_write_compressed (FILE * vtkfile, char *numeric_data, size_t byte_length);

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/


class VTKBinaryEncoder
{
public:
    typedef boost::shared_ptr<VTKBinaryEncoder> Pointer;

    VTKBinaryEncoder();

    virtual ~VTKBinaryEncoder();

    void WriteInt(FILE* pfile, std::vector<int>& r_data);

    void WriteUInt8(FILE* pfile, const std::vector<uint8_t>& r_data);

    void WriteFloat(FILE* pfile, std::vector<float>& r_data);

    std::string Compressor() const;

    void test1();

};

} // Namespace GiD2VTK

#endif

