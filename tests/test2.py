import sys
import os

sys.path.append('/home/hbui/opt/GiD2VTK/lib')
sys.path.append('/home/hbui/opt/GiD2VTK/python')
from GiD2VTK import *
import vtk_utilities
from vtk_utilities import *

# reader = GiDBinaryReader("/home/hbui/workspace/gid/GiD2VTK/data/circular_projectile_test_2_refined_1.00010395.post.bin")
#reader = GiDBinaryReader("/home/hbui/Benchmark_kratos/phase_field_application/3D/circular_projectile_test_2_refined.gid/results_25Jan16_np=64_pffhybrid/circular_projectile_test_2_refined_27.00010395.post.bin")
reader = GiDBinaryReader("../data/circular_projectile_test_2_refined_27.00010395.post.bin")
print(reader)

[coordinates, connectivities] = reader.ReadMesh("Kratos_Tetrahedra3D4_Mesh_1")

[step_list, phase_field] = reader.ReadNodalScalarValues("PHASE_FIELD")

[step_list, displacements] = reader.ReadNodalVectorValues("DISPLACEMENT", 4)

# print(phase_field)
# print(displacements)

writer = VTKUnstructuredGridWriter("../data/circular_projectile_test_2_refined_27.00010395.vtu", "binary", VTKBinaryEncoder())
writer.WriteHeader()
writer.InitializeMesh("Kratos_Tetra", len(coordinates), len(connectivities), 3)
writer.WriteMesh(coordinates, connectivities)
writer.BeginNodalResults()
disp_map = [0, 1, 2]
writer.WriteNodalVectorValues("DISPLACEMENT", displacements, 0, disp_map)
writer.WriteNodalScalarValues("PHASE_FIELD", phase_field, 0)
writer.EndNodalResults()
writer.FinalizeMesh()

