#include "gid_binary_reader.h"

int main(int argc, char** argv)
{
    Kratos::GiDBinaryReader reader("terzaghi_hex27_133326.111.post.bin");

    typedef std::map<int, std::vector<double> > coordinates_container_t;
    typedef std::map<int, std::vector<int> > connectivities_container_t;

    coordinates_container_t Coordinates;
    connectivities_container_t Connectivities;

    reader.ReadMesh(Coordinates, Connectivities);

    std::cout << "Coordinates:" << std::endl;
    for(coordinates_container_t::iterator it = Coordinates.begin(); it != Coordinates.end(); ++it)
    {
        std::cout << it->first << ":";
        for(std::size_t i = 0; i < it->second.size(); ++i)
            std::cout << " " << it->second[i];
        std::cout << std::endl;
    }

    coordinates_container_t Displacements;
    reader.ReadNodalDisplacements(Displacements);
    std::cout << "Displacements:" << std::endl;
    for(coordinates_container_t::iterator it = Displacements.begin(); it != Displacements.end(); ++it)
    {
        std::cout << it->first << ":";
        for(std::size_t i = 0; i < it->second.size(); ++i)
            std::cout << " " << it->second[i];
        std::cout << std::endl;
    }

    return 0;
}

