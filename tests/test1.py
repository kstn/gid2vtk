import sys
import os

sys.path.append('/home/hbui/opt/GiD2VTK/lib')
sys.path.append('/home/hbui/opt/GiD2VTK/python')
from GiD2VTK import *

reader = GiDBinaryReader("../data/terzaghi_hex27_133326.111.post.bin")
print(reader)

[coordinates, connectivities] = reader.ReadMesh("Kratos_Hexahedra3D27_Mesh_1")

[step_list, displacements] = reader.ReadNodalVectorValues("DISPLACEMENT", 4)

print(coordinates)
print(connectivities)
# print(step_list)
# print(displacements)

import vtk_utilities
from vtk_utilities import *
writer = VTKUnstructuredGridWriter("../data/terzaghi_hex27_133326.111.vtu", "binary", VTKBinaryEncoder())
#writer = VTKUnstructuredGridWriter("../data/terzaghi_hex27_133326.111.vtu", "ascii")
writer.WriteHeader()
writer.InitializeMesh("Kratos_Hexahedra3D27_Mesh_1", len(coordinates), len(connectivities), 3)
writer.WriteMesh(coordinates, connectivities)
disp_map = [0, 1, 2]
writer.BeginNodalResults()
writer.WriteNodalVectorValues("DISPLACEMENT", displacements, 0, disp_map)
writer.EndNodalResults()
writer.FinalizeMesh()

enc = VTKBinaryEncoder()
enc.test1()

