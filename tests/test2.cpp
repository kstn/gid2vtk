#include "gid_binary_reader.h"

int main(int argc, char** argv)
{
    Kratos::GiDBinaryReader reader("../data/circular_projectile_test_2_refined_1.00010395.post.bin");
    std::cout << reader << std::endl;

    // read the mesh
//    typedef std::map<int, std::vector<double> > coordinates_container_t;
//    typedef std::map<int, std::vector<int> > connectivities_container_t;
// 
//    coordinates_container_t Coordinates;
//    connectivities_container_t Connectivities;
// 
//    reader.ReadMesh("Kratos_Tetrahedra3D4_Mesh_1", Coordinates, Connectivities);
// 
//    std::cout << "Coordinates:" << std::endl;
//    for(coordinates_container_t::iterator it = Coordinates.begin(); it != Coordinates.end(); ++it)
//    {
//        std::cout << it->first << ":";
//        for(std::size_t i = 0; i < it->second.size(); ++i)
//            std::cout << " " << it->second[i];
//        std::cout << std::endl;
//    }
// 
//    std::cout << "Connectivities:" << std::endl;
//    for(connectivities_container_t::iterator it = Connectivities.begin(); it != Connectivities.end(); ++it)
//    {
//        std::cout << it->first << ":";
//        for(std::size_t i = 0; i < it->second.size(); ++i)
//            std::cout << " " << it->second[i];
//        std::cout << std::endl;
//    }

    // read scalar results PHASE_FIELD
    std::vector<double> step_list;
    std::map<std::size_t, std::vector<double> > Values;
    reader.ReadNodalScalarValues("PHASE_FIELD", step_list, Values);
//    for(std::size_t i = 0; i < step_list.size(); ++i)
//    {
//        std::cout << "PHASE_FIELD at step " << step_list[i] << ", size = " << Values.size() << ":" << std::endl;
//        for(std::map<std::size_t, std::vector<double> >::iterator it = Values.begin(); it != Values.end(); ++it)
//        {
//            std::cout << it->first << ": " << it->second[0] << std::endl;
//        }
//    }

    // read vector results DISPLACEMENT
    std::map<std::size_t, std::vector<std::vector<double> > > Values2;
    reader.ReadNodalVectorValues("DISPLACEMENT", step_list, Values2, 4);
//    for(std::size_t i = 0; i < step_list.size(); ++i)
//    {
//        std::cout << "DISPLACEMENT at step " << step_list[i] << ", size = " << Values2.size() << ":" << std::endl;
//        for(std::map<std::size_t, std::vector<std::vector<double> > >::iterator it = Values2.begin(); it != Values2.end(); ++it)
//        {
//            std::cout << it->first << ":";
//            for(std::size_t i = 0; i < it->second[0].size(); ++i)
//                std::cout << " " << it->second[0][i];
//            std::cout << std::endl;
//        }
//    }

    return 0;
}

