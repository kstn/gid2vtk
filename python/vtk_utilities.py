import sys

class VTKUnstructuredGridWriter():
    def __init__(self, name, wformat, encoder=None):
        self.ifile = open(name, 'w')
        self.is_writing_mesh = False
        self.gid_to_vtk_3d = {}
        self.gid_to_vtk_3d[8]  = [0, 1, 2, 3, 4, 5, 6, 7] # http://www.vtk.org/doc/nightly/html/classvtkHexahedron.html
        self.gid_to_vtk_3d[20] = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 16, 17, 18, 19, 12, 13, 14, 15] # http://www.vtk.org/doc/nightly/html/classvtkQuadraticHexahedron.html
        self.gid_to_vtk_3d[27] = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 16, 17, 18, 19, 12, 13, 14, 15, 24, 22, 21, 23, 20, 25, 26] # http://www.vtk.org/doc/nightly/html/classvtkTriQuadraticHexahedron.html
        self.gid_to_vtk_3d[4]  = [0, 1, 2, 3] # http://www.vtk.org/doc/release/5.2/html/a01371.html
        self.gid_to_vtk_3d[10] = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9] # http://www.vtk.org/doc/release/5.2/html/a01148.html
        self.encoder = encoder
        self.wformat = wformat # "ascii" or "binary"
        if(self.wformat == "binary" and self.encoder == None):
            print("Error: the encoder needs to be provided if the format is binary")
            sys.exit(1)

        # adapt from vtkCellType.h
        self.vtk_type_3d = {4: 10, 8: 12, 10: 24, 20: 25, 27: 29}
        self.vtk_type_2d = {3: 5, 4: 9, 6: 22, 8: 23, 9: 28}

    def __del__(self):
        self.ifile.write('  </UnstructuredGrid>\n')
        self.ifile.write('</VTKFile>\n')
        self.ifile.close()

    def GetFileHandle(self):
        return self.ifile

    def WriteHeader(self):
        self.ifile.write('<?xml version="1.0"?>\n')
        if(self.wformat == "ascii"):
            self.ifile.write('<VTKFile type="UnstructuredGrid" version="0.1">\n')
        elif(self.wformat == "binary"):
            self.ifile.write('<VTKFile type="UnstructuredGrid" version="0.1" byte_order="LittleEndian" compressor="' + self.encoder.Compressor() + '">\n')
        self.ifile.write('  <UnstructuredGrid>\n')

    ## REMAKRS: in the current design, the mesh can carry different element type, but all the nodes must carry the physical values
    def InitializeMesh(self, name, nn, ne, dim):
        if self.is_writing_mesh:
            print("Error: mesh is writing. Cannot initialize a new mesh now")
            sys.exit(1)
        self.is_writing_mesh = True
        self.ifile.write('    <Piece NumberOfPoints="' + str(nn) + '" NumberOfCells="'+ str(ne) + '">\n')
        self.map_node_id = {}
        self.map_elem_id = {}
        self.dim = dim

    def WriteMesh(self, coordinates, connectivities):
        if(not self.is_writing_mesh):
            print("Error: mesh is not initialized")
            sys.exit(1)

        # write points
        self.ifile.write('      <Points>\n')
        self.ifile.write('        <DataArray type="Float32" Name="Position" NumberOfComponents="3" format="' + self.wformat + '">\n')
        cnt = 0
        if self.wformat == "ascii":
            for i, coord in coordinates.iteritems():
                self.ifile.write('          ' + str(coord[0]) + ' ' + str(coord[1]) + ' ' + str(coord[2]) + '\n')
                self.map_node_id[i] = cnt
                cnt = cnt + 1
        elif self.wformat == "binary":
            data_list = []
            self.ifile.write('          ')
            for i, coord in coordinates.iteritems():
                data_list.append(coord[0])
                data_list.append(coord[1])
                data_list.append(coord[2])
                self.map_node_id[i] = cnt
                cnt = cnt + 1
            self.encoder.WriteFloat(self.ifile, data_list)
            self.ifile.write('\n')
        self.ifile.write('        </DataArray>\n')
        self.ifile.write('      </Points>\n')

        # write connectivities
        self.ifile.write('      <Cells>\n')
        self.ifile.write('        <DataArray type="Int32" Name="connectivity" format="' + self.wformat + '">\n')
        offsets = []
        types = []
        if self.dim == 3:
            cnt = 0
            totaln = 0
            if self.wformat == "ascii":
                for elem_id, conn in connectivities.iteritems():
                    nne = len(conn)
                    totaln = totaln + nne
                    offsets.append(totaln)
                    types.append(self.vtk_type_3d[nne])
                    self.ifile.write('         ')
                    for j in range(nne):
                        self.ifile.write(' ' + str(self.map_node_id[conn[self.gid_to_vtk_3d[nne][j]]]))
                    self.ifile.write('\n')
                    self.map_elem_id[cnt] = elem_id
                    cnt = cnt + 1
            elif self.wformat == "binary":
                self.ifile.write('          ')
                data_list = []
                for elem_id, conn in connectivities.iteritems():
                    nne = len(conn)
                    totaln = totaln + nne
                    offsets.append(totaln)
                    types.append(self.vtk_type_3d[nne])
                    for j in range(nne):
                        data_list.append(self.map_node_id[conn[self.gid_to_vtk_3d[nne][j]]])
                    self.map_elem_id[cnt] = elem_id
                    cnt = cnt + 1
                self.encoder.WriteInt(self.ifile, data_list)
                self.ifile.write('\n')
        elif self.dim == 2:
            # TODO
            pass
        self.ifile.write('        </DataArray>\n')
        if self.wformat == "ascii":
            self.ifile.write('        <DataArray type="Int32" Name="offsets" format="ascii">\n')
            self.ifile.write('         ')
            for o in offsets:
                self.ifile.write(' ' + str(o))
        elif self.wformat == "binary":
            self.ifile.write('        <DataArray type="Int32" Name="offsets" format="binary">\n')
            self.ifile.write('          ')
            data_list = []
            for o in offsets:
                data_list.append(o)
#            print("offsets:", data_list)
            self.encoder.WriteInt(self.ifile, data_list)
        self.ifile.write('\n')
        self.ifile.write('        </DataArray>\n')
        if self.wformat == "ascii":
            self.ifile.write('        <DataArray type="UInt8" Name="types" format="ascii">\n')
            self.ifile.write('         ')
            for t in types:
                self.ifile.write(' ' + str(t))
        elif self.wformat == "binary":
            self.ifile.write('        <DataArray type="UInt8" Name="types" format="binary">\n')
            self.ifile.write('          ')
            data_list = []
            for t in types:
                data_list.append(t)
#            print("types:", data_list)
            self.encoder.WriteUInt8(self.ifile, data_list)
        self.ifile.write('\n')
        self.ifile.write('        </DataArray>\n')
        self.ifile.write('      </Cells>\n')

    def Print(self, a):
        print(a)

    def BeginNodalResults(self):
        if(not self.is_writing_mesh):
            print("Error: mesh is not initialized")
            sys.exit(1)
        self.ifile.write('      <PointData>\n')

    def WriteNodalVectorValues(self, name, values, step_id, c_map):
        if(not self.is_writing_mesh):
            print("Error: mesh is not initialized")
            sys.exit(1)
        if(self.wformat == "ascii"):
            self.ifile.write('        <DataArray type="Float32" Name="' + name + '" NumberOfComponents="' + str(len(c_map)) + '" format="ascii">\n')
            for i, val in values.iteritems():
                self.ifile.write('         ')
                for c in c_map:
                    self.ifile.write(' ' + str(val[step_id][c]))
                self.ifile.write('\n')
        elif(self.wformat == "binary"):
            self.ifile.write('        <DataArray type="Float32" Name="' + name + '" NumberOfComponents="' + str(len(c_map)) + '" format="binary">\n')
            self.ifile.write('          ')
            data_list = []
            for i, val in values.iteritems():
                for c in c_map:
                    data_list.append(val[step_id][c])
            self.encoder.WriteFloat(self.ifile, data_list)
            self.ifile.write('\n')
        self.ifile.write('        </DataArray>\n')

    def WriteNodalScalarValues(self, name, values, step_id):
        if(not self.is_writing_mesh):
            print("Error: mesh is not initialized")
            sys.exit(1)
        if(self.wformat == "ascii"):
            self.ifile.write('        <DataArray type="Float32" Name="' + name + '" format="ascii">\n')
            for i, val in values.iteritems():
                self.ifile.write('          ' + str(val[step_id]))
                self.ifile.write('\n')
        elif(self.wformat == "binary"):
            self.ifile.write('        <DataArray type="Float32" Name="' + name + '" format="binary">\n')
            self.ifile.write('           ')
            data_list = []
            for i, val in values.iteritems():
                data_list.append(val[step_id])
            self.encoder.WriteFloat(self.ifile, data_list)
            self.ifile.write('\n')
        self.ifile.write('        </DataArray>\n')

    def EndNodalResults(self):
        if(not self.is_writing_mesh):
            print("Error: mesh is not initialized")
            sys.exit(1)
        self.ifile.write('      </PointData>\n')

    def BeginElementalResults(self):
        if(not self.is_writing_mesh):
            print("Error: mesh is not initialized")
            sys.exit(1)
        self.ifile.write('      <CellData>\n')

    def WriteElementalScalarValues(self, name, values, step_id):
        if(not self.is_writing_mesh):
            print("Error: mesh is not initialized")
            sys.exit(1)
        if(self.wformat == "ascii"):
            self.ifile.write('        <DataArray type="Float32" Name="' + name + '" format="ascii">\n')
            for i in range(0, len(values)):
                val = values[self.map_elem_id[i]]
                self.ifile.write('          ' + str(val[step_id]))
                self.ifile.write('\n')
        elif(self.wformat == "binary"):
            self.ifile.write('        <DataArray type="Float32" Name="' + name + '" format="binary">\n')
            self.ifile.write('           ')
            data_list = []
            for i in range(0, len(values)):
                data_list.append(values[self.map_elem_id[i]][step_id])
            self.encoder.WriteFloat(self.ifile, data_list)
            self.ifile.write('\n')
        self.ifile.write('        </DataArray>\n')

    def EndElementalResults(self):
        if(not self.is_writing_mesh):
            print("Error: mesh is not initialized")
            sys.exit(1)
        self.ifile.write('      </CellData>\n')

    def FinalizeMesh(self):
        self.ifile.write('    </Piece>\n')
        self.is_writing_mesh = False

