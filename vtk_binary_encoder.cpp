#include "vtk_binary_encoder.h"

namespace GiD2VTK
{

int vtk_write_binary(FILE * vtkfile, char *numeric_data, size_t byte_length)
{
    size_t              chunks, chunksize, remaining, writenow;
    size_t              code_length, base_length;
    uint32_t            int_header;
    char               *base_data;
    base64_encodestate  encode_state;

    /* VTK format used 32bit header info */
    assert (byte_length <= (size_t) UINT32_MAX);

    /* This value may be changed although this is not tested with VTK */
    chunksize = (size_t) 1 << 15; /* 32768 */
    int_header = (uint32_t) byte_length;

    /* Allocate sufficient memory for base64 encoder */
    code_length = 2 * std::max (chunksize, sizeof (int_header));
    code_length = std::max (code_length, (std::size_t)4) + 1;
    base_data = (char*) malloc(code_length*sizeof(char));

    base64_init_encodestate (&encode_state);
    base_length = base64_encode_block ((char *) &int_header, sizeof (int_header), base_data, &encode_state);
    assert (base_length < code_length);
    base_data[base_length] = '\0';
    (void) fwrite (base_data, 1, base_length, vtkfile);

    chunks = 0;
    remaining = byte_length;
    while (remaining > 0)
    {
        writenow = std::min (remaining, chunksize);
        base_length = base64_encode_block (numeric_data + chunks * chunksize,
                                           writenow, base_data, &encode_state);
        assert (base_length < code_length);
        base_data[base_length] = '\0';
        (void) fwrite (base_data, 1, base_length, vtkfile);
        remaining -= writenow;
        ++chunks;
    }

    base_length = base64_encode_blockend (base_data, &encode_state);
    assert (base_length < code_length);
    base_data[base_length] = '\0';
    (void) fwrite (base_data, 1, base_length, vtkfile);

    free (base_data);
    if (ferror (vtkfile))
    {
        return -1;
    }
    return 0;
}

void zlib_check(int val)
{
    if(val != Z_OK)
    {
        std::stringstream ss;
        ss << "Error with zlib, error code = " << val;
        throw std::runtime_error(ss.str());
    }
}

int vtk_write_compressed (FILE * vtkfile, char *numeric_data, size_t byte_length)
{
    int                 retval, fseek1, fseek2;
    size_t              iz;
    size_t              blocksize, lastsize;
    size_t              theblock, numregularblocks, numfullblocks;
    size_t              header_entries, header_size;
    size_t              code_length, base_length;
    long                header_pos, final_pos;
    char               *comp_data, *base_data;
    uint32_t           *compression_header;
    uLongf              comp_length;
    base64_encodestate  encode_state;

    /* compute block sizes */
    blocksize = (size_t) (1 << 15);       /* 32768 */
    lastsize = byte_length % blocksize;
    numregularblocks = byte_length / blocksize;
    numfullblocks = numregularblocks + (lastsize > 0 ? 1 : 0);
    header_entries = 3 + numfullblocks;
    header_size = header_entries * sizeof (uint32_t);

    /* allocate compression and base64 arrays */
    code_length = 2 * std::max (blocksize, header_size) + 4 + 1;
    comp_data = (char*) malloc(sizeof(char) * code_length);
    base_data = (char*) malloc(sizeof(char) * code_length);

    /* figure out the size of the header and write a dummy */
    compression_header = (uint32_t*) malloc(sizeof(uint32_t) * header_entries);
    compression_header[0] = (uint32_t) numfullblocks;
    compression_header[1] = (uint32_t) blocksize;
    compression_header[2] = (uint32_t)
    (lastsize > 0 || byte_length == 0 ? lastsize : blocksize);
    for (iz = 3; iz < header_entries; ++iz) {
        compression_header[iz] = 0;
    }
    base64_init_encodestate (&encode_state);
    base_length = base64_encode_block ((char *) compression_header,
                                     header_size, base_data, &encode_state);
    base_length +=
    base64_encode_blockend (base_data + base_length, &encode_state);
    assert (base_length < code_length);
    base_data[base_length] = '\0';
    header_pos = ftell (vtkfile);
    (void) fwrite (base_data, 1, base_length, vtkfile);

    /* write the regular data blocks */
    base64_init_encodestate (&encode_state);
    for (theblock = 0; theblock < numregularblocks; ++theblock) {
        comp_length = code_length;
        retval = compress2 ((Bytef *) comp_data, &comp_length,
                            (const Bytef *) (numeric_data + theblock * blocksize),
                            (uLong) blocksize, Z_BEST_COMPRESSION);
        zlib_check(retval);
        compression_header[3 + theblock] = comp_length;
        base_length = base64_encode_block (comp_data, comp_length,
                                           base_data, &encode_state);
        assert (base_length < code_length);
        base_data[base_length] = '\0';
        (void) fwrite (base_data, 1, base_length, vtkfile);
    }

    /* write odd-sized last block if necessary */
    if (lastsize > 0) {
        comp_length = code_length;
        retval = compress2 ((Bytef *) comp_data, &comp_length,
                            (const Bytef *) (numeric_data + theblock * blocksize),
                            (uLong) lastsize, Z_BEST_COMPRESSION);
        zlib_check(retval);
        compression_header[3 + theblock] = comp_length;
        base_length = base64_encode_block (comp_data, comp_length, base_data, &encode_state);
        assert (base_length < code_length);
        base_data[base_length] = '\0';
        (void) fwrite (base_data, 1, base_length, vtkfile);
    }

    /* write base64 end block */
    base_length = base64_encode_blockend (base_data, &encode_state);
    assert (base_length < code_length);
    base_data[base_length] = '\0';
    (void) fwrite (base_data, 1, base_length, vtkfile);

    /* seek back, write header block, seek forward */
    final_pos = ftell (vtkfile);
    base64_init_encodestate (&encode_state);
    base_length = base64_encode_block ((char *) compression_header, header_size, base_data, &encode_state);
    base_length += base64_encode_blockend (base_data + base_length, &encode_state);
    assert (base_length < code_length);
    base_data[base_length] = '\0';
    fseek1 = fseek (vtkfile, header_pos, SEEK_SET);
    (void) fwrite (base_data, 1, base_length, vtkfile);
    fseek2 = fseek (vtkfile, final_pos, SEEK_SET);

    /* clean up and return */
    free (compression_header);
    free (comp_data);
    free (base_data);
    if (fseek1 != 0 || fseek2 != 0 || ferror (vtkfile)) {
        return -1;
    }

    return 0;
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/


VTKBinaryEncoder::VTKBinaryEncoder()
{}

VTKBinaryEncoder::~VTKBinaryEncoder()
{}  

void VTKBinaryEncoder::WriteInt(FILE* pfile, std::vector<int>& r_data)
{
    int* tmp = (int*)(&r_data[0]);
    #ifdef GID_VTK_USE_ZLIB
    vtk_write_compressed(pfile, (char*)tmp, sizeof(*tmp)*r_data.size());
    #else
    vtk_write_binary(pfile, (char*)tmp, sizeof(*tmp)*r_data.size());
    #endif
}

void VTKBinaryEncoder::WriteUInt8(FILE* pfile, const std::vector<uint8_t>& r_data)
{
    uint8_t* tmp = (uint8_t*)(&r_data[0]);
    #ifdef GID_VTK_USE_ZLIB
    vtk_write_compressed(pfile, (char*)tmp, sizeof(*tmp)*r_data.size());
    #else
    vtk_write_binary(pfile, (char*)tmp, sizeof(*tmp)*r_data.size());
    #endif
}

void VTKBinaryEncoder::WriteFloat(FILE* pfile, std::vector<float>& r_data)
{
    float* tmp = (float*)(&r_data[0]);
    #ifdef GID_VTK_USE_ZLIB
    vtk_write_compressed(pfile, (char*)tmp, sizeof(*tmp)*r_data.size());
    #else
    vtk_write_binary(pfile, (char*)tmp, sizeof(*tmp)*r_data.size());
    #endif
}

std::string VTKBinaryEncoder::Compressor() const
{
    #ifdef GID_VTK_USE_ZLIB
    return "vtkZLibDataCompressor";
    #else
    return "";
    #endif
}

void VTKBinaryEncoder::test1()
{
    float data[] = {5.00000000e-01, 0.00000000e+00, 0.00000000e+00,
                    5.00000000e-01, 5.00000000e-01, 0.00000000e+00,
                    0.00000000e+00, 0.00000000e+00, 0.00000000e+00,
                    0.00000000e+00, 5.00000000e-01, 0.00000000e+00,
                    1.00000000e+00, 0.00000000e+00, 0.00000000e+00,
                    1.00000000e+00, 5.00000000e-01, 0.00000000e+00,
                    5.00000000e-01, 0.00000000e+00, 0.00000000e+00,
                    5.00000000e-01, 5.00000000e-01, 0.00000000e+00,
                    5.00000000e-01, 5.00000000e-01, 0.00000000e+00,
                    5.00000000e-01, 1.00000000e+00, 0.00000000e+00,
                    0.00000000e+00, 5.00000000e-01, 0.00000000e+00,
                    0.00000000e+00, 1.00000000e+00, 0.00000000e+00,
                    1.00000000e+00, 5.00000000e-01, 0.00000000e+00,
                    1.00000000e+00, 7.50000000e-01, 0.00000000e+00,
                    7.50000000e-01, 5.00000000e-01, 0.00000000e+00,
                    7.50000000e-01, 7.50000000e-01, 0.00000000e+00,
                    1.00000000e+00, 7.50000000e-01, 0.00000000e+00,
                    1.00000000e+00, 1.00000000e+00, 0.00000000e+00,
                    7.50000000e-01, 7.50000000e-01, 0.00000000e+00,
                    7.50000000e-01, 1.00000000e+00, 0.00000000e+00,
                    7.50000000e-01, 5.00000000e-01, 0.00000000e+00,
                    7.50000000e-01, 7.50000000e-01, 0.00000000e+00,
                    5.00000000e-01, 5.00000000e-01, 0.00000000e+00,
                    5.00000000e-01, 7.50000000e-01, 0.00000000e+00,
                    7.50000000e-01, 7.50000000e-01, 0.00000000e+00,
                    7.50000000e-01, 1.00000000e+00, 0.00000000e+00,
                    5.00000000e-01, 7.50000000e-01, 0.00000000e+00,
                    5.00000000e-01, 1.00000000e+00, 0.00000000e+00};

//        sample_out = "AQAAAACAAABQAQAARgAAAA==eNqFjtENACEMQtmsjMboetFERM31iwJ9KYDCmsK+I7I+slzef3FCq06mc535aU5N82m+QuvSYfg3Tv7JHw7idvgNNZwV0Q==";

    FILE* f = fopen("./test1.txt", "w");

    if(f != NULL)
    {
        std::vector<float> vdata;
        vdata.assign(data, data+84);
        WATCH(vdata.size());
        WriteFloat(f, vdata);
        fclose(f);
        std::cout << "Test1 completed successfully, output = test1.txt" << std::endl;
    }
    else
        std::cout << "Error creating file test1.txt" << std::endl;
}

} // Namespace GiD2VTK

