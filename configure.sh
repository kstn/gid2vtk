#!/bin/sh
reset
rm CMakeCache.txt
/opt/cmake-3.4.2/bin/cmake .. \
-DCMAKE_C_COMPILER=gcc \
-DCMAKE_CXX_COMPILER=g++ \
-DCMAKE_CXX_FLAGS="${CMAKE_CXX_FLAGS} -std=c++11" \
-DCMAKE_BUILD_TYPE=Release \
-DCMAKE_INSTALL_PREFIX="/home/hbui/opt/GiD2VTK" \
-DZLIB_ROOT="${HOME}/opt/zlib-1.2.8" \
-DBOOST_ROOT="${BOOST_ROOT}" \

make install -j4

